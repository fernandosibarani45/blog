<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>

<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome">
	<label>First name:</label><br><br>
	<input type="text" name="first name"><br><br>
	<label>Last name:</label><br><br>
	<input type="text" name="last name">
	<br><br>
	<label>Gender:</label><br><br>
	<input type="radio" name="Gender">
	<label>Male</label><br>
	<input type="radio" name="Gender">
	<label>Female</label><br>
	<input type="radio" name="Gender">
	<label>Other</label>
	<br><br>
	<label>Nationality</label>
	<br><br>
	<select>
		<option>Indonesian</option>
		<option>Malaysian</option>
		<option>Singapore</option>
		<option>Australian</option>
	</select>
	<br><br>
	<label>Language Spoken</label><br><br>
	<input type="checkbox" name="Bahasa Indonesia">
	<label>Bahasa Indonesia</label><br>
	<input type="checkbox" name="English">
	<label>English</label><br>
	<input type="checkbox" name="Other">
	<label>Other</label><br>
	<br><br>
	<label>Bio:</label><br>
	<textarea cols="25" rows="8"></textarea>
	<br>
	<button type="Submit" Value="Sign Up">Sign Up</button>
</form>

</body>
</html>